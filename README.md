# Fedora Websites FAV Stack
_Development space for FastAPI + Vue3 Fedora websites implementation_

## Frontend Notes
- To run a dev server:
  - Navigate to `client/`
  - run `npm run serve` to fire up development server
- For background resources on working in Vue, see `client/README.md`

### Plugins
_Not all are necessary, but would make life easier_
- [i18next](https://www.i18next.com/): internationalization
- [i18-vue](https://kazupon.github.io/vue-i18n/): Vue internationalization
- [Vuex](https://vuex.vuejs.org/): state management and global asset store
- [Vueuse](https://vueuse.org/): metadata and a bunch of other useful features
- [Vue Router](https://router.vuejs.org/): client side routing
- [Vue Axios](https://github.com/imcvampire/vue-axios#readme): Simplifies axios calls
- [Vue Fontawesome](https://fontawesome.com/docs/web/use-with/vue/): Icons

### Articles
- [Bootstrap 5 and Vue 3 Stackoverflow Post](https://stackoverflow.com/questions/65547199/using-bootstrap-5-with-vue-3): Describes how to work with bootstrap 5 and vue 3 (which has changed from prior versions)
- [Internationalization with Vue and i18n](https://dev.to/adrai/how-to-properly-internationalize-a-vue-application-using-i18next-1doj)
- [Building an app with FastAPI and Vue](https://testdriven.io/blog/developing-a-single-page-app-with-fastapi-and-vuejs/)
---

## Backend Notes

