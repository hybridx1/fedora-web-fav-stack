# Frontend for Fedora Websites

## Getting Started
- Access components in the `src/components` directory
- Pages can be found in `src/views`
- Navigation can be edited in `src/router/index.ts`

### Background Information
- [Typescript Basics](https://www.typescriptlang.org/docs/handbook/2/basic-types.html)
- [VueJS Props](https://vuejs.org/guide/components/props.html): Passing information from parent to child components
- [VueJS Slots](https://vuejs.org/guide/components/slots.html#slots): Creat space in components for content to be added, add content using slots
- [Composition API](https://vuejs.org/api/composition-api-setup.html#composition-api-setup)
- [Vue Router](https://router.vuejs.org/guide/): Basics of routing in a Vue application
- [Vue Templating Directives](https://vuejs.org/api/built-in-directives.html)(Pay attention to the following in particular):
  - `v-bind`
  - `v-for`
  - `v-on`
  - `v-slot`
  - `v-if` / `v-else` / `v-else-if`
- [Vue Single File Component Syntax](https://vuejs.org/api/sfc-spec.html)
- [SSR in Vue](https://vuejs.org/api/ssr.html) For creating a SSR App **NOTE**: Example uses Express server. Ours might not be exactly like these docs.
- [Vue School App Structure](https://vueschool.io/articles/vuejs-tutorials/how-to-structure-a-large-scale-vue-js-application/): Some helpful notes
- [Vue Component Organization](https://vueschool.io/articles/vuejs-tutorials/structuring-vue-components/): Some guidelines to help plan out or components effectively.

### Component Notes
- `components/layout/` are components used for overal page structure. Things like navs, footers, and main sections that structure how the overal page is designed go here. Most of these will not take any props or slots and will be basically the same everywhere they are used. Prefixed with `The` are one time use per page, when using a general `App`, they are meant to be reusable.
- `components/utlities` are presentational components. They provide styling and responsive behaviour but have no actual content. Add value to them with props and slots. Things like buttons, cards, lists go here. 

---

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
