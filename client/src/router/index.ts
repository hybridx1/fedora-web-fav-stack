import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import GetFedora from "../views/GetFedora.vue";
import PageNotFound from "../views/PageNotFound.vue"
import StartFedora from "../views/StartFedora.vue"

// TODO: Plan out nesting strategy and consider breaking up routes into multiple files for neater organization

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Get Fedora",
    component: GetFedora,
    /* External redirect in same tab -- Use after global navigation is created for incremental adoption of new vue system
    beforeEnter(to, from, next) {
      window.location.href = "https://getfedora.org"
    }
    */
  },
  {
    path: "/start",
    name: "Start",
    component: StartFedora,
    /* External redirect in same tab -- Use after global navigation is created for incremental adoption of new vue system
    beforeEnter(to, from, next) {
      window.location.href = "https://start.fedoraproject.org"
    }
    */
  },
  // Editions Pages
  {
    path: "/editions/workstation",
    name: "Workstation",
    component: () => import("../views/editions/workstation/Index.vue")
  },
  // TODO: Create structure sections with their children more effectively
  {
    path: "/editions/iot",
    name: "IoT",
    component: () => import("../views/editions/Iot.vue"),
    /* External redirect in same tab -- Use after global navigation is created for incremental adoption of new vue system
    beforeEnter(to, from, next) {
      window.location.href = "https://getfedora.org/iot"
    }
    */
  },
  {
    path: "/editions/server",
    name: "Server",
    component: () => import("../views/editions/Server.vue")
  },
  // Emerging Editions Pages
  {
    path: "/emerging-editions/coreos",
    name: "Core OS",
    component: () => import("../views/emerging-editions/Coreos.vue")
  },
  {
    path: "/emerging-editions/silverblue",
    name: "Silverblue",
    component: () => import("../views/emerging-editions/Silverblue.vue")
  },
  // Labs
  {
    path: "/labs/games",
    name: "Games",
    component: () => import("../views/labs/Games.vue")
  },
  {
    path: "/labs/scientific",
    name: "Scientific",
    component: () => import("../views/labs/Scientific.vue")
  },
  // spins
  {
    path: "/spins/kde",
    name: "KDE",
    component: () => import("../views/spins/Kde.vue")
  },
  {
    path: "/spins/xfce",
    name: "XFCE",
    component: () => import("../views/spins/Xfce.vue")
  },
  // Page not Found -- Keep as last element in the router
  {
    path: "/:catchAll(.*)*",
    name: "PageNotFound",
    component: PageNotFound
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
