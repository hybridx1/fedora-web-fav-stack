// core library 
import { library } from '@fortawesome/fontawesome-svg-core'

// icon component
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// import solid icons
import { faArrowRight } from '@fortawesome/free-solid-svg-icons'
// import brand icons
import { faFedora, faYoutube} from '@fortawesome/free-brands-svg-icons'

// Add to library
library.add(faFedora, faYoutube, faArrowRight )

export default FontAwesomeIcon